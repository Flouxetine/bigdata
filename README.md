# 大数据开发代码
分类补充大数据开发java代码
## hadoop2.7.2
hdfs java api操作，查看创建删除列出文件信息\n  
文件上传下载\n  
io操作文件上传下载\n  
自定义bean文件实现mapreduce\n  
MapReduce中InputFormat的实现类：\n  
   0.InputFormat(getSplits,getRecordReader)\n  
        *1.FileInputFormat\n  
            *1.1.TextInputFormat\n  
                *1.1.1.CombineTextInputFormat\n  
                *1.1.2.NLineTextInputFormat\n  
                *1.1.3.KeyValueTextInputFormat\n  
## hbase
java api   
guli weibo
## kafka
topic client api  
topic api  
consumer api  
producer发送方式  

## flink
## spark
RDD的创建方式  
RDD的算法因子 
makeRDD和testFile源码对RDD分区的说明  
1，makeRDD  
makeRDD底层调用的是parallelize()  
private object ParallelCollectionRDD是RDD分区的逻辑代码  
def slice[T: ClassTag](seq: Seq[T], numSlices: Int): Seq[Seq[T]]  
seq match {  
case _ =>  
val array = seq.toArray // To prevent O(n^2) operations for List etc  
positions(array.length, numSlices).map { case (start, end) =>  
array.slice(start, end).toSeq  
}.toSeq  
def positions(length: Long, numSlices: Int): Iterator[(Int, Int)] = {  
(0 until numSlices).iterator.map { i =>  
第一次循环--0  
第二次循环--1  
第三次循环--2
val start = ((i * length) / numSlices).toInt  
第一次循环--0*5/3=0  
第二次循环--1*5/3=1  
第三次循环--2*5/3=3  
val end = (((i + 1) * length) / numSlices).toInt  
第一次循环--(0+1)*5/3=1  
第二次循环--(1+1)*5/3=3
第三次循环--(1+2)*5/3=5  
(start, end)  
第一次循环--[0,1)  
第二次循环--[1,3)  
第三次循环--[3,5)  
以一个makeRDD(List(1,2,3,4,5),3)为例    
RDD的length为5  
numSlices为3  
positions(5,3)  
0-->[0,1)-->1   
1-->[1,3)-->2,3  
2-->[3,5)-->4,5  
元素：1，2，3，4，5    
下标：0，1，2，3，4  

## storm

## flume


