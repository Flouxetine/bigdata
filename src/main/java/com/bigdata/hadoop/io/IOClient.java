package com.bigdata.hadoop.io;

import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.conf.Configuration;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;

public class IOClient {

    public FileSystem initFileSystem() throws URISyntaxException {
        URI uri = new URI("hdfs://hzzx:9000");
        String hdfsUser = "root";
        FileSystem fileSystem = null;
        Configuration configuration = new Configuration();
        try {
            fileSystem = FileSystem.get(uri,configuration,hdfsUser);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return fileSystem;
    }

    /**
     * io拷贝文件到hdfs
     */
    public void putFileToHDFS(){
        try {
            FileSystem fs = initFileSystem();
            FileInputStream inputStream = new FileInputStream(new File("/Users/yahve/IdeaProjects/bigdata/src/main/resources/input/wc.txt"));
            FSDataOutputStream fsDataOutputStream = fs.create(new Path("bigdata/hadoop/wc.txt"));
            IOUtils.copyBytes(inputStream,fsDataOutputStream,fs.getConf());
            IOUtils.closeStream(inputStream);
            IOUtils.closeStream(fsDataOutputStream);
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * io从hdfs拷贝文件到本地
     * @throws URISyntaxException
     * @throws IOException
     */
    public void getFileFromHDFS() throws URISyntaxException, IOException {
        FileSystem fileSystem = initFileSystem();
        FSDataInputStream fsDataInputStream = fileSystem.open(new Path("/user/root/bigdata/hadoop/wc.txt"));
        OutputStream outputStream = new FileOutputStream(new File("/Users/yahve/IdeaProjects/bigdata/src/main/resources/output/wc-out/wc.txt"));
        IOUtils.copyBytes(fsDataInputStream,outputStream,fileSystem.getConf());
        IOUtils.closeStream(fsDataInputStream);
        IOUtils.closeStream(outputStream);
    }

    /**
     * 从hdfs上下载指定文件大小的文件
     * @throws URISyntaxException
     * @throws IOException
     */
    public void getFileFromHDFSParts() throws URISyntaxException, IOException {
        FileSystem fs = initFileSystem();
        FSDataInputStream dataInputStream = fs.open(new Path("/bigdata/hadoop/hive"));
        FileOutputStream fileOutputStream = new FileOutputStream(new File("/Users/yahve/IdeaProjects/bigdata/src/main/resources/output/io-part/hive-part1"));
        //第一分部
        byte[] bytes = new byte[1024];
        for (int i = 0 ; i<=10248128 ; i++){
            dataInputStream.read(bytes);
            fileOutputStream.write(bytes);
        }
        dataInputStream.close();
        fileOutputStream.close();

    }

}
