package com.bigdata.hadoop.mr.join;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


import java.awt.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class MapSideJoinMapper extends Mapper<LongWritable, Text,Text, NullWritable> {

    Map<String,String> hashMap = new HashMap<>();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {

        URI[] cacheFiles = context.getCacheFiles();
        String string = cacheFiles[0].getPath().toString();
        //读取缓冲中的文件
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(string), "utf-8"));
        String line ;
        while (!StringUtils.isEmpty(line=reader.readLine())){
            String[] strings = line.split("\t");
            hashMap.put(strings[0],strings[1]);
        }

    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        Text k = new Text();
        String[] orders = value.toString().split("\t");
        String pid = orders[1];
        String pName = hashMap.get(pid);
        if (hashMap.containsKey(pid)){
            String ll = orders[0]+"\t"+pName+"\t"+orders[2];
            k.set(ll);
            context.write(k,NullWritable.get());
        }

    }
}
