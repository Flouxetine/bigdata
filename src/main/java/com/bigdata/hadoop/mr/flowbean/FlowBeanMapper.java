package com.bigdata.hadoop.mr.flowbean;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class FlowBeanMapper extends Mapper<LongWritable, Text,Text,FlowBean> {

    Text text = new Text();
    FlowBean flowBean = new FlowBean();
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String[] fields = value.toString().split("\t");
        long upFlow = Long.parseLong(fields[fields.length-3]);
        long downFlow = Long.parseLong(fields[fields.length-2]);
        String phoneNum = fields[1];

        text.set(phoneNum);
        flowBean.setUpFlow(upFlow);
        flowBean.setDownFlow(downFlow);
        context.write(text,flowBean);
    }
}
