package com.bigdata.hadoop.mr.inputformat;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.lib.CombineFileInputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import javax.lang.model.SourceVersion;
import javax.tools.Tool;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;

public class CombineTextInputFormatDriver implements Tool {
    private Configuration configuration = null;

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public int run(InputStream in, OutputStream out, OutputStream err, String... arguments) {
        configuration = this.getConfiguration();
        boolean isCompletion = false;
        try {
            Job job = Job.getInstance(configuration);
            job.setJarByClass(CombineTextInputFormatDriver.class);

            job.setMapperClass(CombineTextInputFormatMapper.class);
            job.setReducerClass(CombineTextInputFormatReducer.class);

            job.setInputFormatClass(CombineFileInputFormat.class);
            CombineFileInputFormat.setMaxInputSplitSize(job,4194304);

            job.setMapOutputKeyClass(LongWritable.class);
            job.setMapOutputValueClass(Text.class);

            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(IntWritable.class);

            this.initJobInputPath(job);
            this.initJobOutputPath(job);

            isCompletion = job.waitForCompletion(true);


        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return isCompletion ? 0 : 1;
    }

    private void initJobInputPath(Job job) throws IOException {

        Configuration configuration = new Configuration();
        String inPathString = configuration.get("inpath");
        FileSystem fs = FileSystem.get(configuration);
        Path inPath = new Path(inPathString);
        if (fs.exists(inPath)){
            FileInputFormat.setInputPaths(job,inPath);
        }else {
            System.out.println("文件不存在");
        }


    }

    private void initJobOutputPath(Job job) throws IOException {

        Configuration configuration = job.getConfiguration();
        String outPathString = configuration.get("outpath");
        FileSystem fs = FileSystem.get(configuration);
        Path outPath = new Path(outPathString);
        if (fs.exists(outPath)){
            fs.delete(outPath);
        }
        FileOutputFormat.setOutputPath(job,outPath);

    }

    @Override
    public Set<SourceVersion> getSourceVersions() {
        return null;
    }
}
