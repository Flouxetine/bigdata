package com.bigdata.hadoop.mr.inputformat;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class NLTextInputFormatMapper extends Mapper<LongWritable, Text,Text,LongWritable> {

    Text k = new Text();
    LongWritable v = new LongWritable(1);
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String[] words = value.toString().split(" ");
        for (int i = 0; i < words.length; i++) {
            k.set(words[1]);
        }
        context.write(k,v);
    }
}
