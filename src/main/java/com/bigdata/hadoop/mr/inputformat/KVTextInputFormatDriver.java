package com.bigdata.hadoop.mr.inputformat;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class KVTextInputFormatDriver {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {

        args = new String[]{"/Users/yahve/IdeaProjects/bigdata/src/main/resources/input/kv-input.txt",
            "/Users/yahve/IdeaProjects/bigdata/src/main/resources/output/kv-output"};
        Configuration configuration = new Configuration();
        Job job = Job.getInstance(configuration);

        job.setInputFormatClass(KeyValueTextInputFormat.class);
        job.setJarByClass(KVTextInputFormatDriver.class);
        job.setMapperClass(KVTextInputFormatMapper.class);
        job.setReducerClass(KVTextInputFormatReducer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(LongWritable.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);

        FileInputFormat.setInputPaths(job,new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        boolean waitForCompletion = job.waitForCompletion(true);
        System.out.println(waitForCompletion);
    }
}
