package com.bigdata.hadoop.mr.inputformat;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class CombineTextInputFormatMapper extends Mapper<LongWritable, Text,Text , IntWritable> {

    Text text = new Text();
    IntWritable v = new IntWritable();
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String[] fides = value.toString().split(",");
        for (String fide : fides) {
            text.set(fide);
        }
        context.write(text,v);


    }
}
