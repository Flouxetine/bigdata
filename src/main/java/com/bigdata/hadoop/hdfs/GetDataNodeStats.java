package com.bigdata.hadoop.hdfs;

import com.bigdata.hadoop.io.IOClient;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.apache.hadoop.hdfs.protocol.DatanodeInfo;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class GetDataNodeStats {
    IOClient ioClient = new IOClient();

    /**
     * 获取hadoop集群实时状态
     * @throws URISyntaxException
     * @throws IOException
     */
    public void getDataNodeStatus() throws URISyntaxException, IOException {
        FileSystem fs = ioClient.initFileSystem();
        DistributedFileSystem hdfs = (DistributedFileSystem) fs;
        DatanodeInfo[] dataNodeStats = hdfs.getDataNodeStats();

        for (int i = 0; i < dataNodeStats.length; i++) {

            String report = dataNodeStats[i].getDatanodeReport();
            System.out.println(report);
        }
    }

    @Test
    public void testGetDataNodeStatus() throws IOException, URISyntaxException {
        getDataNodeStatus();
    }
}
