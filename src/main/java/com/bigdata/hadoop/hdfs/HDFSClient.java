package com.bigdata.hadoop.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.fs.permission.FsPermission;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class HDFSClient {

    private static Configuration conf = new Configuration();
    private static FileSystem fs = new FilterFileSystem();

    static {
        try {
            fs = FileSystem.get(new URI("hdfs://hzzx:9000"), conf, "root");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除hdfs上的文件
     * @param path
     * @throws IOException
     */
    public void deleteFileFromHDFS(Path path) throws IOException {
        try {
            if (fs.exists(path)){
                fs.delete(path,true);
            }else {
                System.err.println("file is not exists");
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }

    /**
     * 创建文件
     * @param path
     * @throws IOException
     */
    public void createFileOnHDFS(Path path) throws IOException {
        if(!fs.exists(path)){
            fs.create(path);

        }else {
            System.err.println("the file is already exists");
        }

    }

    /**
     * 从本地拷贝文件到hdfs
     * @param localPath
     * @param HDFSPath
     */
    public void copyFromLocalFile(Path localPath,Path HDFSPath){
        try {
            if(fs.exists(HDFSPath)){
                fs.copyFromLocalFile(localPath,HDFSPath);
            }else {
                System.err.println("file is not exists");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 从hdfs上将文件拷贝到本地
     * @param localPath
     * @param HDFSPath
     */
    public void copyToLocalFile(Path HDFSPath ,Path localPath) throws IOException {
        if (fs.exists(HDFSPath)){
            fs.copyToLocalFile(HDFSPath,localPath);
        }else {
            System.err.println("file is not exists");
        }

    }

    /**
     * 列出hdfs文件
     */
    public void listFile(){
        try {
            RemoteIterator<LocatedFileStatus> files = fs.listFiles(new Path("/"), true);
            while(files.hasNext()){
                LocatedFileStatus fileStatus = files.next();
                String fileName = fileStatus.getPath().getName();
                long len = fileStatus.getLen();
                String group = fileStatus.getGroup();
                String owner = fileStatus.getOwner();
                FsPermission permission = fileStatus.getPermission();
                //文件名
                System.out.println("fileName:"+fileName);
                //组
                System.out.println("group:"+group);
                //所有者
                System.out.println("owner:"+owner);
                //长度
                System.out.println("len:"+len);
                System.out.println("permission:"+permission);
                BlockLocation[] blockLocations = fileStatus.getBlockLocations();
                for (BlockLocation blockLocation:blockLocations){
                    //块存储位置
                    String[] hosts = blockLocation.getHosts();
                    for (String host:hosts){
                        System.out.println("host:"+host);
                    }
                }
                System.out.println("--------------------------------");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
