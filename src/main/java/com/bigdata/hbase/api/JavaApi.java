package com.bigdata.hbase.api;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HBaseAdmin;

import java.io.IOException;

public class JavaApi {

    /**
     * 初始化hbase连接
     * @return
     */
    public Configuration initHBase(){
        Configuration configuration = null;
        configuration = HBaseConfiguration.create();
        configuration.set("hbase.zookeeper.quorum","10.211.55.7");
        configuration.set("hbase.zookeeper.property.clientPort","2181");
        configuration.set("hbase.master","10.211.55.7:9001");
        return configuration;
    }

    /**
     * 判断标明是否存在
     * @param tableName
     * @return
     * @throws IOException
     */
    public boolean isTableExist(String tableName) throws IOException {
        Configuration configuration = initHBase();
        HBaseAdmin hBaseAdmin = new HBaseAdmin(configuration);
        boolean tableExists = hBaseAdmin.tableExists(tableName);
        return tableExists;
    }


}
