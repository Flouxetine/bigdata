package com.bigdata.hbase.giliweibo.dao;

import com.bigdata.hbase.giliweibo.constants.Constants;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.ArrayList;

public class HBaseDao {

    /**
     * 发布微博
     * 先发布微博，然后查找微博用户粉丝，构建集合，向粉丝收件箱表插入数据
     * @param uid
     * @param content
     * @throws IOException
     */
    public static void publishContent(String uid,String content) throws IOException {

        Connection connection = ConnectionFactory.createConnection(Constants.CONFIGURATION);
        //发布微博
        Table contTable = connection.getTable(TableName.valueOf(Constants.CONTENT_TABLE));
        long currentTimeMillis = System.currentTimeMillis();
        String rowKey = uid +"_"+ currentTimeMillis;
        Put contPut = new Put(Bytes.toBytes(rowKey));
        contPut.addColumn(Bytes.toBytes(Constants.CONTENT_TABLE_CF),
                Bytes.toBytes("content"),Bytes.toBytes(content));
        contTable.put(contPut);
        //微博用户粉丝
        Table relationTable = connection.getTable(TableName.valueOf(Constants.RELATION_TABLE));
        Get get = new Get(Bytes.toBytes(uid));
        get.addFamily(Bytes.toBytes(Constants.RELATION_TABLE_CF2));
        Result result = relationTable.get(get);
        ArrayList<Put> inboxPuts = new ArrayList<>();
        for (Cell cell : result.rawCells()) {
            Put inboxPut = new Put(CellUtil.cloneQualifier(cell));
            inboxPut.addColumn(Bytes.toBytes(Constants.INBOX_TABLE_CF), Bytes.toBytes(uid),Bytes.toBytes(rowKey));
            inboxPuts.add(inboxPut);
        }
        //粉丝收件箱表插入数据
        if (inboxPuts.size()>0){
            Table inboxTable = connection.getTable(TableName.valueOf(Constants.INBOX_TABLE));
            inboxTable.put(inboxPuts);
            inboxTable.close();
        }

        contTable.close();
        relationTable.close();
        connection.close();
    }

    /**
     * 关注微博
     * @param uid
     * @param attends
     * @throws IOException
     */
    public static void addAttends(String uid,String... attends) throws IOException {

        Connection connection = ConnectionFactory.createConnection(Constants.CONFIGURATION);
        Table relationTable = connection.getTable(TableName.valueOf(Constants.RELATION_TABLE));
        ArrayList<Put> relationPuts = new ArrayList<>();
        Put uidPut = new Put(Bytes.toBytes(uid));
        for (String attend : attends) {
            uidPut.addColumn(Bytes.toBytes(Constants.RELATION_TABLE_CF1),Bytes.toBytes(attend),Bytes.toBytes(attend));

            Put attendPut = new Put(Bytes.toBytes(attend));
            attendPut.addColumn(Bytes.toBytes(Constants.RELATION_TABLE_CF2),Bytes.toBytes(uid),Bytes.toBytes(uid));
            relationPuts.add(attendPut);
        }
        relationPuts.add(uidPut);
        relationTable.put(relationPuts);
    }
}
