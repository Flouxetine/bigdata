package com.bigdata.hbase.giliweibo.utils;

import com.bigdata.hbase.giliweibo.constants.Constants;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;

import java.io.IOException;

public class HBaseUtil {

    /**
     * 创建命名空间
     * @param nameSpace
     */
    public static void createNameSpace(String nameSpace){
        try {
            Connection connection = ConnectionFactory.createConnection(Constants.CONFIGURATION);
            Admin admin = connection.getAdmin();
            NamespaceDescriptor namespaceDescriptor = NamespaceDescriptor.create(nameSpace).build();
            admin.createNamespace(namespaceDescriptor);
            admin.close();
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 判断表是否存在
     * @param tableName
     * @return
     */
    private static boolean isTableExists(String tableName){
        Connection connection = null;
        boolean tableExists = false;
        try {
            connection = ConnectionFactory.createConnection(Constants.CONFIGURATION);
            Admin admin = connection.getAdmin();
            tableExists = admin.tableExists(TableName.valueOf(tableName));
            admin.close();
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (tableExists){
        return true;
        }else {
            return false;
        }
    }

    /**
     * 创建表的公共类
     * @param tableName
     * @param versions
     * @param cfs
     * @throws IOException
     */
    private static void createTable(String tableName,int versions, String... cfs) throws IOException {

        if (cfs.length<=0){
         System.out.println("请设置列祖信息!!!");
         return;
        }
        if (isTableExists(tableName)){
            System.out.println(tableName+"表已存在!!!");
            return;
        }
        Connection connection = ConnectionFactory.createConnection(Constants.CONFIGURATION);
        Admin admin = connection.getAdmin();
        HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf(tableName));
        for (String cf : cfs) {
            HColumnDescriptor hColumnDescriptor = new HColumnDescriptor(cf);
            hColumnDescriptor.setMaxVersions(versions);
            hTableDescriptor.addFamily(hColumnDescriptor);
        }
        admin.createTable(hTableDescriptor);
        admin.close();
        connection.close();

    }
}
