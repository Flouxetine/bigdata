package com.bigdata.spark.rdd

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
object AgentRDDDemo {
  /**
   * 时间戳 省份 城市 用户 广告
   * 1516609143867 6 7 64 16
   * 1516609143869 9 4 75 18
   * 1516609143869 1 7 87 12
   * 1516609143869 2 8 92 9
   * 1516609143869 6 7 84 24
   * @param args
   */
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("agent")
    val sc = new SparkContext(conf)
    val file = sc.textFile("/Users/yahve/IdeaProjects/bigdata/src/main/resources/input/agent.log")

    //现将数据读入为((省份,广告),1)
    val mapRDD : RDD[((String,String),Int)] = file.map(x => {
      val datas = x.split(" ")
      ((datas(1), datas(4)), 1)
    })
    //再以(城市,广告)聚合
    val sumRDD : RDD[((String,String),Int)] = mapRDD.reduceByKey(_ + _)
    //再将((城市,广告),sum)改成(城市,(广告,sum))
    val reduceRDD : RDD[(String,(String,Int))] = sumRDD.map {
      case ((a, b), c) => (a, (b, c))
    }
    //按省份分组
    val groupRDD : RDD[(String,Iterable[(String,Int)])] = reduceRDD.groupByKey()
    //排序取前三
    groupRDD.mapValues(iter => {
      iter.toList.sortBy(_._2)(Ordering.Int.reverse).take(3)
    }).collect().foreach(println)
    sc.stop()
  }

}
