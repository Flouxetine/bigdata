package com.bigdata.spark.rdd;

import org.apache.commons.net.io.Util;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.rdd.RDD;

import java.util.*;
import java.util.Arrays;
import java.util.List;

public class TransRDD {

    /**
     * 初始化sc
     * @return
     */
    public JavaSparkContext initSparkContext(){
        JavaSparkContext sc = new JavaSparkContext("local", "distinct");
        return sc;
    }

    /**
     * distinct
     */
    public void scDistinct(){
        JavaSparkContext sc = initSparkContext();
        List<Integer> disList = Arrays.asList(1, 2, 3, 4, 6, 2, 5, 4);
        JavaRDD<Integer> javaRDD = sc.parallelize(disList);
        JavaRDD<Integer> distinct = javaRDD.distinct();
        System.out.println(distinct.collect());
    }

    /**
     * 两种抽样方式
     * 泊松
     * 伯努利
     */
    public void scSample(){
        JavaSparkContext sc = initSparkContext();
        JavaRDD<Integer> javaRDD = sc.parallelize(Arrays.asList(1, 2, 3, 5, 6, 7));
        JavaRDD<Integer> sampleBS = javaRDD.sample(true, 1);
        JavaRDD<Integer> sampleBML = javaRDD.sample(false, 0.5);
        System.out.println("泊松："+sampleBS.collect());
        System.out.println("伯努利："+sampleBML.collect());
    }

    /**
     * 有时候spark在运行job的时候，会产生很多小文件或是空文件，这时候我们需要将输出的partition进行重新调整
     * 可以减少RDD中partition的数目，
     * 有两种方式
     * coalesce(numPartitions: Int): JavaRDD[T] = rdd.coalesce(numPartitions)
     * repartition(numPartitions: Int): JavaRDD[T] = rdd.repartition(numPartitions)
     * repartition是coalesce的简化版
     */
    public void scCoalesce(){
        JavaSparkContext sc = initSparkContext();
        //构建一个1到100 的list
        ArrayList<Integer> ints = new ArrayList<Integer>();
        for (int i = 1;i<=100;i++){
            ints.add(i);
        }
        JavaRDD<Integer> rdd = sc.parallelize(ints,10);
        JavaRDD<Integer> coalesce = rdd.coalesce(5);
        System.out.println(coalesce.partitions().toArray().length);

    }

    public void scRepartition(){
        JavaSparkContext sc = initSparkContext();
        //构建一个1到100 的list
        ArrayList<Integer> ints = new ArrayList<Integer>();
        for (int i = 1;i<=100;i++){
            ints.add(i);
        }
        JavaRDD<Integer> rdd = sc.parallelize(ints,10);
        JavaRDD<Integer> coalesce = rdd.repartition(5);
        System.out.println(coalesce.partitions().toArray().length);

    }

    public void scSortBy(){
        JavaSparkContext sc = initSparkContext();
        List<Integer> list = Arrays.asList(2, 4, 7, 1, 5, 8);
        JavaRDD<Integer> rdd = sc.parallelize(list);
        JavaRDD<Integer> sortBy = rdd.sortBy(x->x,false,2);
        System.out.println(sortBy.collect());

    }
    //双value值

    /**
     * 求两个RDD的并集
     */
    public void scIntersection(){
        JavaSparkContext sc = initSparkContext();
        JavaRDD<Integer> javaRDD1 = sc.parallelize(Arrays.asList(1, 2, 4, 5),1);
        JavaRDD<Integer> javaRDD2 = sc.parallelize(Arrays.asList(2, 3, 5),1);
        JavaRDD<Integer> result = javaRDD1.intersection(javaRDD2);
        result.foreach(new VoidFunction<Integer>() {
            @Override
            public void call(Integer integer) throws Exception {
                System.out.println(integer);
            }
        });
    }

    /**
     * 求两个RDD之间的差集
     */
    public void scSubtract(){
        JavaSparkContext sc = initSparkContext();
        JavaRDD<Integer> javaRDD1 = sc.parallelize(Arrays.asList(1, 2, 4, 5),1);
        JavaRDD<Integer> javaRDD2 = sc.parallelize(Arrays.asList(2, 3, 5),1);
        JavaRDD<Integer> subtract = javaRDD1.subtract(javaRDD2);
        System.out.println(subtract.collect());
    }
    public void scUnion(){
        JavaSparkContext sc = initSparkContext();
        JavaRDD<Integer> javaRDD1 = sc.parallelize(Arrays.asList(1, 2, 4, 5),2);
        JavaRDD<Integer> javaRDD2 = sc.parallelize(Arrays.asList(2, 3, 5),1);
        JavaRDD<Integer> union = javaRDD1.union(javaRDD2);
        System.out.println(union.collect());
    }

    /**
     * 两个rdd的类型不一定要相同，但是rdd元素个数必须相同
     */
    public void scZip(){
        JavaSparkContext sc = initSparkContext();
        JavaRDD<Integer> javaRDD1 = sc.parallelize(Arrays.asList(1, 2, 4, 5),1);
        JavaRDD<String> javaRDD2 = sc.parallelize(Arrays.asList("2", "3", "5","8"),1);
        JavaPairRDD<Integer, String> union = javaRDD1.zip(javaRDD2);
        System.out.println(union.collect());
    }
    // partitionBy


    //reduceBy
    public void scReduceBy(){
        JavaSparkContext sc = initSparkContext();
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("A",1);
        hashMap.put("B",2);
        hashMap.put("C",3);
        
    }
    



}
