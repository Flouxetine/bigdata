package com.bigdata.spark.rdd

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object CreateRDD {
  /**
   * 创建RDD的四种方式
   * @param args
   */
  def main(args: Array[String]): Unit = {

    //从集合中创建RDD，spark提供两种方式
    val sparkConf = new SparkConf().setMaster("local[*]").setAppName("spark")
    val sparkContext = new SparkContext(sparkConf)
    //1.makeRDD
    val RDDDemo1 = sparkContext.makeRDD(List(1,2,3,4,5))
    //1.parallelize
    val RDDDemo2 = sparkContext.parallelize(List(2,4,6,8,10))
    //2.从外部文件创建RDD
    val RDDDemo3 : RDD[String] = sparkContext.textFile("/Users/yahve/IdeaProjects/bigdata/src/main/resources/input/order.txt")
    //3.获取上一步已经存在的RDD
    //4.直接new
    //new RDD[]() {}
    RDDDemo1.collect().foreach(print)
    println()
    RDDDemo2.collect().foreach(print)
    println()
    RDDDemo3.collect().foreach(print)
    sparkContext.stop()
  }

}
