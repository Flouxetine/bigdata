package com.bigdata.spark.rdd

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object TransRDD01 {
  def main(args: Array[String]): Unit = {

    val conf = new SparkConf().setMaster("local[*]").setAppName("transform")
    val sc = new SparkContext(conf)
    //map
    //将处理的数据逐条进行映射转换，这里的转换可以是类型的转换，也可以是值的转换
    var dataRDD : RDD[Int] = sc.makeRDD(List(1,2,4,6),1)
    val arrayString = Array("A", "B", "C", "D", "E", "F")
    //val tuplesString = arrayString.map((_,1))
    val tuplesString = arrayString.map(x=>{(x,1)})
    //单Array构建map
//    tuplesString.foreach(println)
    //直接运算数据
    val mapRDD = dataRDD.map(x => {
      x * 2
    })
//    mapRDD.collect().foreach(println)
    //mapPartitions
    //将待处理的数据以分区为单位发送到计算节点进行处理，这里的处理是指可以进行任意的处理，哪怕是过滤数据
    val mapPRDD = dataRDD.mapPartitions(date => {
      date.filter(_%2 == 0)
    })
//    mapPRDD.collect().foreach(println)
    //mapPartitionsWithIndex
//    val valueRDD = dataRDD.mapPartitionsWithIndex((index, datas) => {
//      datas.map(index, _)
//    })
    val mpwi : RDD[Int] = sc.makeRDD(List(1, 2, 3, 4, 5, 6, 7, 8), 2)
    def function0(index:Int,iter:Iterator[Int]):Iterator[String]={
      iter.toList.map(x=>"[partID:"+index+",value="+x+"]").iterator
    }
//    mpwi.mapPartitionsWithIndex(function0).collect().foreach(println)

    //flatMap
    //扁平化处理
    val flatMapRDD = sc.parallelize(List(List(1,2),List(3,4)))
//    flatMapRDD.flatMap(list=>list).collect().foreach(print)
    //扁平化比较
    //flatMap扁平话意思大概就是先用了一次map之后对全部数据再一次map。
//    sc.parallelize(Array(("A",1),("B",2),("C",3))).flatMap(x=>(x._1+x._2)).collect().foreach(println)
//    sc.parallelize(Array(("A",1),("B",2),("C",3))).map(x=>(x._1+x._2)).collect().foreach(println)

    //glom
    //将数据分入到分区后计算
    val glomRDD = sc.parallelize(List(1, 2, 3, 4, 5, 6, 7, 8, 9), 3)
//    glomRDD.collect()的结果是
//    Array[int] = Array(1,2,3,4,5,6,7,8,9)
//    glomRDD.glom().collect()的结果
//    Array[int] = Array(Array(1,2,3),Array(4,5,6),Array(7,8,9))


    //groupBy
//    val groupData : RDD[Int]= sc.makeRDD(List(1, 2, 3, 4, 5, 6, 7, 8, 9))
//    groupData.groupBy(_).collect().foreach(print)

    //sample
    //
//    val sampleData : RDD[Int]= sc.makeRDD(List(1, 2, 3, 4, 5, 6, 7, 8, 9))
//    //泊松
//    sampleData.sample(true,1).collect().foreach(print)
//    println("")
//    //伯努利
//    sampleData.sample(false,0.5,1).collect().foreach(print)
    val distinctData = sc.makeRDD(List(1,2,3,4,5,2,4))
    val ff = distinctData.distinct(2)
      ff.collect().foreach(print)
   }
}
