package com.bigdata.spark.rdd

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object PartitionRDD {
  /**
   *默认情况下，spark可以将一个作业切分成多个任务，发送给executor节点并行执行
   * 而能够并行计算任务的数量称为并行度，这个是在创建RDD的时候创建的
   * @param args
   */
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("partition").setMaster("local[*]")
    val sc = new SparkContext(conf)

    val dataRDD : RDD[Int] = sc.makeRDD(List(1, 2, 3, 4), 4)
    val fileRDD : RDD[String] = sc.textFile("/Users/yahve/IdeaProjects/bigdata/src/main/resources/input/flowbean.txt",
      2)
    fileRDD.collect().foreach(println)
    sc.stop()

  }

}
