package com.bigdata.kafka.message.consumer;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.TopicPartition;

import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

public class CustomConsumer {
    public void getAsyncConsumer(){

        Properties prop = new Properties();

        prop.put("bootstrap.servers","hzzx:9092");
        prop.put("group.id","meisTest");
        prop.put("enable.auto.commit","false");
        prop.put("key.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        prop.put("value.deserializer","org.apache.kafka.common.serialization.StringDeserializer");

        KafkaConsumer<Object, Object> consumer = new KafkaConsumer<>(prop);
        consumer.subscribe(Arrays.asList("meis"));

        try {

            while (true) {
                ConsumerRecords<Object, Object> records = consumer.poll(100);
                for (ConsumerRecord<Object, Object> record : records){
                    System.out.println("topic==>"+record.topic()+",key==>"+record.key()+",value==>"+record.value());
                }
                consumer.commitAsync
                        (new OffsetCommitCallback(){
                            @Override
                            public void onComplete(Map<TopicPartition, OffsetAndMetadata> offsets, Exception exception) {
                                if (exception!=null){
                                    System.err.println("commit faild "+ offsets);
                                }
                            }
                        });

            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            consumer.close();
        }

    }
    public void getSyncConsumer(){
        Properties prop = new Properties();
        prop.put("bootstrap.servers","hzzx:9092");
        prop.put("group.id","meisTest1");
        //自动提交设置为false
        prop.put("enable.auto.commit","false");

        prop.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        prop.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(prop);
        kafkaConsumer.subscribe(Arrays.asList("meis"));

        while (true){
            ConsumerRecords<String, String> records = kafkaConsumer.poll(100);

            for (ConsumerRecord<String,String> record : records){
                System.out.printf("offset = %d,key  = %s, value = %s%n", record.offset(), record.key(),record.value());
            }
            try {
                //处理完当前的消息，在轮询更多消息之前，调用commitAsync提交当前批次最新的消息
                kafkaConsumer.commitAsync();
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                kafkaConsumer.close();
            }

        }

    }

}
