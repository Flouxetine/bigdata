package com.bigdata.kafka.message.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class CustomProducer {
    private static final Properties prop = new Properties();

    /**
     * 创建异步发送消息的Producer
     */
    /**
     * 异步发送
     */
    public static void createCustomProducerAsync(){
        //集群的broker-list
        prop.put("bootstrap.servers","hzzx:9092");
        prop.put("acks","all");
        //重试次数
        prop.put("retries",1);
        //批次大小
        prop.put("batch.size",16438);
        //等待时间
        prop.put("linger.ms",1);
        //缓冲区大小
        prop.put("buffer.memory",33554432);

        prop.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        prop.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(prop);
        for (int i = 1;i<10;i++){
            kafkaProducer.send(new ProducerRecord<String, String>("meis", Integer.toString(i)), (recordMetadata, e) -> {
                if (e == null){
                    System.out.println("succcess==>" + recordMetadata.offset());
                }else {
                    e.printStackTrace();
                }
            });
        }
        kafkaProducer.close();

    }


    /**
     * 创建同步发送消息的producer
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static void createCustomProducerSync() throws ExecutionException, InterruptedException {
        prop.put("bootstrap.servers","hzzx:9092");
        prop.put("acks","all");
        //重试次数
        prop.put("retries",1);
        //批次大小
        prop.put("batch.size",16438);
        //等待时间
        prop.put("linger.ms",1);
        //缓冲区大小
        prop.put("buffer.memory",33554432);
        prop.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        prop.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(prop);
        for (int i = 0; i < 5; i++) {
            kafkaProducer.send(new ProducerRecord<String,String>("",Integer.toString(i))).get();
        }
    }
}
