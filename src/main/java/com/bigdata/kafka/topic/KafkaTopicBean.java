package com.bigdata.kafka.topic;

public class KafkaTopicBean {
    private String topicName;
    private Integer partition;
    private Integer replication;

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public Integer getPartition() {
        return partition;
    }

    public void setPartition(Integer partition) {
        this.partition = partition;
    }

    public Integer getReplication() {
        return replication;
    }

    public void setReplication(Integer replication) {
        this.replication = replication;
    }

    @Override
    public String toString() {
        return "KafkaTopicBean{" +
                "topicName='" + topicName + '\'' +
                ", partition=" + partition +
                ", replication=" + replication +
                '}';
    }
}
