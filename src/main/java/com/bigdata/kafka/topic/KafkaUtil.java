package com.bigdata.kafka.topic;

import com.bigdata.kafka.constants.Constants;
import kafka.admin.AdminUtils;
import kafka.admin.RackAwareMode;
import kafka.server.ConfigType;
import kafka.utils.ZkUtils;
import org.apache.kafka.common.security.JaasUtils;
import scala.collection.JavaConversions;

import java.util.*;

public class KafkaUtil {



    public static ZkUtils initZKUtils(String zkStr){
        ZkUtils zkUtils = ZkUtils.apply(zkStr, 30000, 30000, JaasUtils.isZkSecurityEnabled());
        return zkUtils;
    }
    /**
     * 创建topic
     * @param zkStr
     * @param topic
     */
    public static void createKafkaTopic(String zkStr,KafkaTopicBean topic){

        ZkUtils zkUtils = ZkUtils.apply(zkStr, 30000, 30000, JaasUtils.isZkSecurityEnabled());
        AdminUtils.createTopic(zkUtils, topic.getTopicName(), topic.getPartition(),
                topic.getReplication(), new Properties(), RackAwareMode.Enforced$.MODULE$);
        zkUtils.close();
    }

    /**
     * 删除topic
     * @param zkStr
     * @param topic
     */
    public static void deleteKafkaTopic(String zkStr,KafkaTopicBean topic){
        ZkUtils zkUtils = ZkUtils.apply(zkStr,30000,30000,JaasUtils.isZkSecurityEnabled());

        AdminUtils.deleteTopic(zkUtils,topic.getTopicName());
        zkUtils.close();

    }

    /**
     * 查询指定topic的属性
     * @param zkStr
     * @param topicName
     */
    public static void queryKafkaTopicByTopicName(String zkStr,String topicName){

        ZkUtils zkUtils = ZkUtils.apply(zkStr, 30000, 30000, JaasUtils.isZkSecurityEnabled());
        Properties properties = AdminUtils.fetchEntityConfig(zkUtils, ConfigType.Topic(), topicName);

        Iterator<Map.Entry<Object, Object>> iterator = properties.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry entry = iterator.next();
            Object key = entry.getKey();
            Object value = entry.getValue();
            System.out.println("key:"+key+"=="+"value"+value);
        }
        zkUtils.close();

    }

    /**
     * 获取topic列表信息
     * @param zkStr
     */
    public static List<String> queryKafkaTopic(String zkStr){

        ZkUtils zkUtils = ZkUtils.apply(zkStr, 30000, 30000, JaasUtils.isZkSecurityEnabled());
        List<String> topics = JavaConversions.seqAsJavaList(zkUtils.getAllTopics());
        return topics;
    }


    /**
     * 修改kafka属性
     * @param zkStr
     * @param topicName
     */
    public static void updateTopicConfig(String zkStr,String topicName){

        ZkUtils zkUtils = ZkUtils.apply(zkStr, 30000, 30000, JaasUtils.isZkSecurityEnabled());
        Properties kafkaConfigs = AdminUtils.fetchEntityConfig(zkUtils, ConfigType.Topic(), topicName);
        //增加topic日志清理策略
        kafkaConfigs.put("min.cleanable.dirty.ratio","0.3");
//        kafkaConfigs.remove();
        AdminUtils.changeTopicConfig(zkUtils,topicName,kafkaConfigs);
        zkUtils.close();

    }

    /**
     * 获取所有的消费者信息
     * @return
     */
    public static List<String> getKafkaConsumer(){
        String zkUtil = Constants.ZOOKEEPERSTRING;
        ZkUtils zkUtils = initZKUtils(zkUtil);
        List<String> kafkaConsumers = JavaConversions.seqAsJavaList(zkUtils.getConsumerGroups());
        return kafkaConsumers;
    }

    /**
     * 根据消费者名称查询topic
     * @param consumerGroup
     * @return
     */
    public static List<String> getTopicsByConsumerGroup(String consumerGroup){
        String zkUtil = Constants.ZOOKEEPERSTRING;
        ZkUtils zkUtils = initZKUtils(zkUtil);
        List<String> topics = JavaConversions.seqAsJavaList(zkUtils.getTopicsByConsumerGroup(consumerGroup));
        return topics;
    }

    /**
     * 获取topicName下所有的消费者信息
     * @param topicNmae
     * @return
     */
    public static List<String> getAllConsumerGroupsForTopic(String topicNmae){
        String zkUtil = Constants.ZOOKEEPERSTRING;
        ZkUtils zkUtils = initZKUtils(zkUtil);
        List<String> consumers = (List<String>)zkUtils.getAllConsumerGroupsForTopic(topicNmae);
        return consumers;
    }

    /**
     * 获取topic的路径
     * @param topicName
     * @return
     */
    public static String getTopicPath(String topicName){
        String zkString = Constants.ZOOKEEPERSTRING;
        ZkUtils zkUtils = initZKUtils(zkString);
        String topicPath = zkUtils.getTopicPath(topicName);
        return topicPath;
    }
}
