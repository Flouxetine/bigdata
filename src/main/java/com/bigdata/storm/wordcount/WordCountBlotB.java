package com.bigdata.storm.wordcount;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;

import java.util.HashMap;
import java.util.Map;

public class WordCountBlotB extends BaseRichBolt {
    OutputCollector collector;
    Map<String,Integer> map = new HashMap<String,Integer>();
    @Override
    public void prepare(Map<String, Object> map, TopologyContext topologyContext, OutputCollector outputCollector) {

        this.collector = collector;
    }

    @Override
    public void execute(Tuple tuple) {

        String word = tuple.getString(0);
        Integer num = tuple.getInteger(1);
        if (map.containsKey(word)){
            Integer nums = map.get(word);
            map.put(word,nums+num);
        }else {
            map.put(word,1);
        }
        System.out.println("count--" + map);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {

    }
}
