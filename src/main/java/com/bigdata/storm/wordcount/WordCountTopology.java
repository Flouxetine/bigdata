package com.bigdata.storm.wordcount;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;

public class WordCountTopology {

    public static void main(String[] args) throws Exception {

        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("DemoSpout",new WordCountSpout(),1);
        builder.setBolt("BoltA",new WordCountBoltA(),2).shuffleGrouping("DemoSpout");
        builder.setBolt("BoltB",new WordCountBlotB(),2).fieldsGrouping("BoltA",new Fields("word"));

        Config config = new Config();
        config.setNumWorkers(2);

//        LocalCluster localCluster = new LocalCluster();
//
//        localCluster.submitTopology("DemoWordCount",config,builder.createTopology());

        StormSubmitter.submitTopology("DemoWordCount",config,builder.createTopology());

    }
}
