package kafka;

import com.bigdata.kafka.message.consumer.CustomConsumer;
import com.bigdata.kafka.message.producer.CustomProducer;
import com.bigdata.kafka.topic.KafkaTopicBean;
import com.bigdata.kafka.topic.KafkaUtil;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.bigdata.kafka.message.producer.CustomProducer.createCustomProducerAsync;
import static com.bigdata.kafka.message.consumer.CustomConsumer.*;

public class TestKafkaUtil {
    CustomConsumer consumer = new CustomConsumer();
//    new void CustomProducer()
    private String zkStr = "hzzx:2181/kafka";
    @Test
    public void textKafkaUtil(){

        KafkaTopicBean topic = new KafkaTopicBean();
        topic.setTopicName("testTopic");
        topic.setPartition(1);
        topic.setReplication(1);

//        KafkaUtil.createKafkaTopic(zkStr,topic);

        KafkaUtil.queryKafkaTopicByTopicName(zkStr,"testTopic");
    }
    @Test
    public void testQueryKafkaTopic(){
        KafkaUtil.queryKafkaTopic(zkStr);
    }
    @Test
    public void testGetTopicPath(){
        String meis = KafkaUtil.getTopicPath("meis");
        System.out.println(meis);
    }
    @Test
    public void testGetAllConsumerGroupsForTopic(){
        List<String> meis = KafkaUtil.getAllConsumerGroupsForTopic("meis");
        for (String mei : meis) {
            System.out.println(mei);
        }
    }
    @Test
    public void testCreateCustomProducerSync() throws ExecutionException, InterruptedException {
//        createCustomProducerAsync();
        consumer.getSyncConsumer();
    }

}
