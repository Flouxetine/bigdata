package hbase;

import com.bigdata.hbase.api.JavaApi;
import org.junit.Test;

import java.io.IOException;

public class TestApi {

    @Test
    public void testTableExist(){
        JavaApi api = new JavaApi();
//        api.initHBase();
        try {
            boolean device = api.isTableExist("device");
            System.out.println(device);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
