package hadoop;

import com.bigdata.hadoop.hdfs.HDFSClient;
import com.bigdata.hadoop.io.IOClient;
import org.apache.hadoop.fs.Path;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class TestHDFSClient {

    HDFSClient hdfsClient = new HDFSClient();
    IOClient ioClient = new IOClient();
    @Test
    public void testDeleteFromHDFS(){
        Path path = new Path("/bigdata/hadoop/text.txt");

        try {
            hdfsClient.deleteFileFromHDFS(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCreateFileOnHDFS(){
        Path path = new Path("bigdata/hadoop/hdfsclient");
        try {
            hdfsClient.createFileOnHDFS(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testListFiles(){
        hdfsClient.listFile();
    }

    @Test
    public void testPutFileToHDFS() throws IOException, URISyntaxException {
//        ioClient.putFileToHDFS();
        ioClient.getFileFromHDFS();
    }
    @Test
    public void testGetFileFromHDFSParts() throws IOException, URISyntaxException {
        ioClient.getFileFromHDFSParts();
    }
}
